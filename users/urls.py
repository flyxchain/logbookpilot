from django.contrib import admin
from django.urls import path
from users import views

urlpatterns = [
    path('users', views.welcome),
    path('register', views.register),
    path('login', views.login),
    path('logout', views.logout),
]