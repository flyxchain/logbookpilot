from django.shortcuts import render, redirect

# Create your views here.

def homepage(request):
    # Si estamos identificados devolvemos la portada
    return render(request, "index.html")
